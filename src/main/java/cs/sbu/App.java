package cs.sbu;


import cs.sbu.air.*;

import java.util.LinkedList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        List<Shape> shapes = new LinkedList<>();
        shapes.add(new Circle(1));
        shapes.add(new Circle(3));
        shapes.add(new Square(2));
        shapes.add(new Triangle(1, 2, 2.5));
        shapes.add(new Triangle(2, 2, 2.5));
        shapes.add(new Sphere(3));
        shapes.add(new Sphere(1.1));
        shapes.add(new Cylinder(1, 1));
        shapes.add(new Cylinder(1, 3));
        shapes.add(new Cylinder(2, 3));
        for (Shape shape : shapes) {
            System.out.println(shape);
        }

        Circle circle1 = new Circle(2);
        Circle circle2 = new Circle(2);
        System.out.println(circle1 == circle2);
        System.out.println(circle1.equals(circle2));
    }
}
