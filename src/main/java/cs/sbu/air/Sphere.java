package cs.sbu.air;

public class Sphere extends ThreeDimShape {

    private double radius;

    public Sphere(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return 4 * Utils.PI * this.radius * this.radius;
    }

    @Override
    public double getVolume() {
        return 4 / 3 * Utils.PI * Math.pow(this.radius, 3);
    }

    @Override
    public String toString() {
        return "Sphere{" +
                "radius=" + radius +
                ", volume=" + this.getVolume() +
                ", area=" + this.getArea() +
                '}';
    }
}
