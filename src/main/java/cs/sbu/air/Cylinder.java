package cs.sbu.air;

public class Cylinder extends ThreeDimShape {

    private double radius;
    private double height;

    public Cylinder(double radius, double height) {
        this.radius = radius;
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getArea() {
        return 2 * Utils.PI * this.radius * this.height + 2 * Utils.PI * this.radius * this.radius;
    }

    @Override
    public double getVolume() {
        return Utils.PI * Math.pow(this.radius, 2) * height;
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "radius=" + radius +
                ", height=" + height +
                ", volume=" + this.getVolume() +
                ", area=" + this.getArea() +
                '}';
    }
}
