package cs.sbu.air;

public class Square extends TwoDimShape {

    private double zel;

    public Square(double zel) {
        this.zel = zel;
    }

    public double getZel() {
        return zel;
    }

    public void setZel(double zel) {
        this.zel = zel;
    }

    @Override
    public double getArea() {
        return this.zel * this.zel;
    }

    @Override
    public double getPerimeter() {
        return 4 * this.zel;
    }

    @Override
    public String toString() {
        return "Square{" +
                "zel=" + zel +
                ", perimeter=" + this.getPerimeter() +
                ", area=" + this.getArea() +
                '}';
    }
}
