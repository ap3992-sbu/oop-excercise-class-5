package cs.sbu.air;

public class Circle extends TwoDimShape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Utils.PI * this.radius * this.radius;
    }

    @Override
    public double getPerimeter() {
        return Utils.PI * 2 * this.radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", perimeter=" + this.getPerimeter() +
                ", area=" + this.getArea() +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Circle) {
            Circle tmp = (Circle) obj;
            return this.radius == tmp.getRadius();
        }
        return false;
    }
}
